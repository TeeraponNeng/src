import Customers from '../views/customers/customers';
import ShowCustomers from '../views/customers/showCustomers';
import ShowImportData from '../views/importdata/show-importdata';
import SendParcel from '../views/parcel/send-parcel';

/*--------------------------------------------------------------------------------*/
/*                                  Dashboards                                    */
/*--------------------------------------------------------------------------------*/
import FirstDashboard from '../views/dashboards/dashboard1';
import SecondDashboard from '../views/dashboards/dashboard2';
import ThirdDashboard from '../views/dashboards/dashboard3';
import FourthDashboard from '../views/dashboards/dashboard4';

import AuthRoutes from './authroutes';

var auths = [].concat(AuthRoutes);

var ThemeRoutes = [
	/*{
		navlabel: true,
		name: 'Personal',
		icon: 'mdi mdi-dots-horizontal'
	},*/
	{
		navShow: true,
		path: '/send-parcel',
		name: 'ส่งพัสดุ',
		icon: 'mdi mdi-truck-delivery',
		component: SendParcel,
	},
	{
		navShow: true,
		path: '/showCustomers',
		name: 'ข้อมูลลูกค้า',
		icon: 'mdi mdi-account-circle',
		component: ShowCustomers,
	},
	{
		navShow: false,
		navlabel: false,
		path: '/customers',
		name: "CreateCustomer",
		component: Customers
	},
	{
		navShow: true,
		path: '/show-importdata',
		name: 'นำเข้าข้อมูล',
		icon: 'mdi mdi-database',
		component: ShowImportData,
	},
	{
		collapse: true,
		navShow: true,
		path: '/dashboards',
		name: 'Dashboards',
		state: 'dashboardpages',
		icon: 'mdi mdi-gauge',
		child: [
			{
				path: '/dashboards/dashboard1',
				name: 'Dashboard 1',
				mini: 'B',
				icon: 'mdi mdi-adjust',
				component: FirstDashboard
			},
			{
				path: '/dashboards/dashboard2',
				name: 'Dashboard 2',
				mini: 'B',
				icon: 'mdi mdi-adjust',
				component: SecondDashboard
			},
			{
				path: '/dashboards/dashboard3',
				name: 'Dashboard 3',
				mini: 'B',
				icon: 'mdi mdi-adjust',
				component: ThirdDashboard
			},
			{
				path: '/dashboards/dashboard4',
				name: 'Dashboard 4',
				mini: 'B',
				icon: 'mdi mdi-adjust',
				component: FourthDashboard
			}
		]
	},
	{
		navShow: true,
		path: '/authentication',
		name: 'Authentication',
		state: 'openAuthentication',
		icon: 'mdi mdi-account-circle',
		child: auths,
		collapse: true
	},
	{
		navShow: true,
		path: '/',
		pathTo: '/dashboards/dashboard1',
		name: 'Dashboard',
		redirect: true
	}
];
export default ThemeRoutes;
