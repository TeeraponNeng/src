import React from 'react';
import { connect } from 'react-redux';
import {
	Nav,
	NavItem,
	NavLink,
	Button,
	Navbar,
	NavbarBrand,
	Collapse,
	UncontrolledDropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem,
} from 'reactstrap';
import * as data from './data.js';

/*--------------------------------------------------------------------------------*/
/* Import images which are need for the HEADER                                    */
/*--------------------------------------------------------------------------------*/
import logodarkicon from '../../../assets/images/logo/logo-icon.png';
import logolighticon from '../../../assets/images/logo/logo-icon.png';
import logodarktext from '../../../assets/images/logo/logo-text.png';
import logolighttext from '../../../assets/images/logo/logo-text.png';
import profilephoto from '../../../assets/images/users/1.jpg';

const mapStateToProps = state => ({
	...state
});

class Header extends React.Component {
	constructor(props) {
		super(props);
		this.toggle = this.toggle.bind(this);
		this.showMobilemenu = this.showMobilemenu.bind(this);
		this.sidebarHandler = this.sidebarHandler.bind(this);
		this.state = {
			isOpen: false
		};
		this.toggleMenu = this.toggleMenu.bind(this);
	}
	/*--------------------------------------------------------------------------------*/
	/*To open Search Bar                                                              */
	/*--------------------------------------------------------------------------------*/
	toggleMenu() {
		document.getElementById('search').classList.toggle('show-search');
	}
	/*--------------------------------------------------------------------------------*/
	/*To open NAVBAR in MOBILE VIEW                                                   */
	/*--------------------------------------------------------------------------------*/
	toggle() {
		this.setState({
			isOpen: !this.state.isOpen
		});
	}
	/*--------------------------------------------------------------------------------*/
	/*To open SIDEBAR-MENU in MOBILE VIEW                                             */
	/*--------------------------------------------------------------------------------*/
	showMobilemenu() {
		document.getElementById('main-wrapper').classList.toggle('show-sidebar');
	}
	sidebarHandler = () => {
		let element = document.getElementById('main-wrapper');
		switch (this.props.settings.activeSidebarType) {
			case 'full':
			case 'iconbar':
				element.classList.toggle('mini-sidebar');
				if (element.classList.contains('mini-sidebar')) {
					element.setAttribute('data-sidebartype', 'mini-sidebar');
				} else {
					element.setAttribute(
						'data-sidebartype',
						this.props.settings.activeSidebarType
					);
				}
				break;

			case 'overlay':
			case 'mini-sidebar':
				element.classList.toggle('full');
				if (element.classList.contains('full')) {
					element.setAttribute('data-sidebartype', 'full');
				} else {
					element.setAttribute(
						'data-sidebartype',
						this.props.settings.activeSidebarType
					);
				}
				break;

			default:
		}
	};

	render() {
		return (
			<header
				className="topbar navbarbg"
				data-navbarbg={this.props.settings.activeNavbarBg}
			>
				<Navbar
					className={
						'top-navbar ' +
						(this.props.settings.activeNavbarBg === 'skin6'
							? 'navbar-light'
							: 'navbar-dark')
					}
					expand="md"
				>
					<div
						className="navbar-header"
						id="logobg"
						data-logobg={this.props.settings.activeLogoBg}
					>
						<span
							className="nav-toggler d-block d-md-none text-white"
							onClick={this.showMobilemenu}
						>
							<i className="ti-menu ti-close" />
						</span>
						<NavbarBrand href="/">
							<b className="logo-icon">
								<img src={logodarkicon} alt="homepage" className="dark-logo" />
								<img
									src={logolighticon}
									alt="homepage"
									className="light-logo"
								/>
							</b>
							<span className="logo-text">
								<img src={logodarktext} alt="homepage" className="dark-logo" />
								<img
									src={logolighttext}
									className="light-logo"
									alt="homepage"
								/>
							</span>
						</NavbarBrand>
						<span
							className="topbartoggler d-block d-md-none text-white"
							onClick={this.toggle}
						>
							<i className="ti-more" />
						</span>
					</div>
					<Collapse
						className="navbarbg"
						isOpen={this.state.isOpen}
						navbar
						data-navbarbg={this.props.settings.activeNavbarBg}>
						<Nav className="float-left" navbar>
							<NavItem>
								<NavLink
									href="#"
									className="d-none d-md-block"
									onClick={this.sidebarHandler}
								>
									<i className="ti-menu" />
								</NavLink>
							</NavItem>
						</Nav>
						<Nav className="ml-auto float-right" navbar>
							<UncontrolledDropdown nav inNavbar>
								<DropdownToggle nav caret className="pro-pic">
									<img
										src={profilephoto}
										alt="user"
										className="rounded-circle"
										width="31"
									/>
								</DropdownToggle>
								<DropdownMenu right className="user-dd">
									<div className="d-flex no-block align-items-center p-3 mb-2 border-bottom">
										<div className="">
											<img
												src={profilephoto}
												alt="user"
												className="rounded"
												width="80"
											/>
										</div>
										<div className="ml-3">
											<h4 className="mb-0">Steave Jobs</h4>
											<p className="text-muted mb-0">varun@gmail.com</p>
											<Button color="danger" className="btn-rounded mt-2">
												View Profile
                      </Button>
										</div>
									</div>
									<DropdownItem>
										<i className="ti-user mr-1 ml-1" /> บัญชีของฉัน
                  </DropdownItem>
									<DropdownItem href="/pages/login">
										<i className="fa fa-power-off mr-1 ml-1" /> ออกจากระบบ
                  </DropdownItem>
								</DropdownMenu>
							</UncontrolledDropdown>
						</Nav>
					</Collapse>
				</Navbar>
			</header>
		);
	}
}
export default connect(mapStateToProps)(Header);
