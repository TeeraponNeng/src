import React from 'react';
class Footer extends React.Component {
  render() {
    return (
      <footer className="footer text-center">
        Copyright © 2020 PORLOR EXPRESS.CO.TH All Rights Reserved.
      </footer>
    );
  }
}
export default Footer;
