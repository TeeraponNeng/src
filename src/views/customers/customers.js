import React from 'react';
import {
    Card,
    CardBody,
    CardTitle,
    Row,
    Col,
    Form,
    FormGroup,
    Label,
    Input,
    FormText,
    Button,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    InputGroupButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    FormFeedback,
    CustomInput
} from 'reactstrap';

class Customers extends React.Component {
    render() {
        return (
            <Row>
                <Col sm="12">
                    <Card>
                        <CardTitle className="p-3 border-bottom mb-0">
                            <i className="fas fa-user-plus" />
                            <strong> เพิ่มข้อมูลลูกค้า</strong>

                        </CardTitle>
                        <CardBody>
                            <Form>
                                <Row>
                                    <Col md="12">
                                        <Label>ประเภทลูกค้า</Label>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md="12">
                                        <CustomInput inline type="radio" id="rdoCustBusiness" name="rdoCustType" label="ลูกค้าธุรกิจ" />
                                        <CustomInput inline type="radio" id="rdoCustCredit" name="rdoCustType" label="ลูกค้าเครดิต" />
                                        <CustomInput inline type="radio" id="rdoCustGeneral" name="rdoCustType" label="ลูกค้าทั่วไป" />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md="4">
                                        <FormGroup>
                                            <Label>ชื่อลูกค้า</Label>
                                            <Input type="text" placeholder="" />
                                        </FormGroup>
                                    </Col>
                                    <Col md="4">
                                        <FormGroup>
                                            <Label>นามสกุล</Label>
                                            <Input type="text" placeholder="" />
                                        </FormGroup>
                                    </Col>
                                    <Col md="4">
                                        <FormGroup>
                                            <Label>เลขประจำตัวผู้เสียภาษีอากร</Label>
                                            <Input type="text" placeholder="" />
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md="12">
                                        <FormGroup>
                                            <Label>ที่อยู่</Label>
                                            <Input type="text" placeholder="" />
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md="4">
                                        <FormGroup>
                                            <Label>ตำบล/แขวง</Label>
                                            <Input type="text" placeholder="" />
                                        </FormGroup>
                                    </Col>
                                    <Col md="4">
                                        <FormGroup>
                                            <Label>อำเภอ/เขต</Label>
                                            <Input type="text" placeholder="" />
                                        </FormGroup>
                                    </Col>
                                    <Col md="2">
                                        <FormGroup>
                                            <Label>จังหวัด</Label>
                                            <Input type="text" placeholder="" />
                                        </FormGroup>
                                    </Col>
                                    <Col md="2">
                                        <FormGroup>
                                            <Label>รหัสไปรษณีย์</Label>
                                            <Input type="text" placeholder="" />
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md="4">
                                        <FormGroup>
                                            <Label>เบอร์โทรศัพท์</Label>
                                            <Input type="text" placeholder="" />
                                        </FormGroup>
                                    </Col>
                                    <Col md="4">
                                        <FormGroup>
                                            <Label>เบอร์โทรศัพท์มือถือ</Label>
                                            <Input type="text" placeholder="" />
                                        </FormGroup>
                                    </Col>
                                    <Col md="4">
                                        <FormGroup>
                                            <Label>วันที่่ลงทะเบียน</Label>
                                            <Input type="date" placeholder="DOB Here" />
                                        </FormGroup>
                                    </Col>
                                </Row>

                            </Form>
                        </CardBody>
                        <CardBody className="bg-light">
                            <CardTitle className="mb-0">ผู้ประสานงาน/ผู้ติดต่อขอลงทะเบียน</CardTitle>
                        </CardBody>
                        <CardBody>
                            <Row>
                                <Col md="2">
                                    <FormGroup>
                                        <Label>คำนำหน้าชื่อ</Label>
                                        <Input type="select" name="ddlInitial">
                                            <option>--คำนำหน้าชื่อ--</option>
                                            <option>นาย</option>
                                            <option>นาง</option>
                                            <option>นางสาว</option>
                                        </Input>
                                    </FormGroup>
                                </Col>
                                <Col md="4">
                                    <FormGroup>
                                        <Label>ชื่อผู้ประสานงาน</Label>
                                        <Input type="text" placeholder="" />
                                    </FormGroup>
                                </Col>
                                <Col md="4">
                                    <FormGroup>
                                        <Label>นามสกุล</Label>
                                        <Input type="text" placeholder="" />
                                    </FormGroup>
                                </Col>
                            </Row>
                        </CardBody>
                        <CardBody className="border-top">
                            <Button type="submit" className="btn btn-success"> <i className="fas fa-save"></i> บันทึก</Button>
                            <Button type="button" className="btn btn-dark ml-2"><i className="fas fa-redo"></i> ล้างข้อมูล</Button>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default Customers;
