import React from 'react';
import ReactTable from "react-table";
import {
    Row,
    Col,
    Card,
    CardBody,
    CardTitle,
    Button, Modal, ModalHeader, ModalBody, Form, FormGroup, Label, Input
} from 'reactstrap';
import "react-table/react-table.css";
import * as data from "./reactable-data";

class ShowImportData extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            obj: {},
            jsonData: data.dataTable,
            data: data.makeData(),
            treedata: data.treedata
        };
        this.toggle = this.toggle.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        let id = event.target.id.value;
        let name = event.target.name.value;
        let designation = event.target.designation.value;
        let location = event.target.location.value;
        let age = event.target.age.value;
        let newObj = JSON.parse(JSON.stringify(this.state.jsonData));
        newObj[id] = [name, designation, location, age];
        this.setState({
            jsonData: newObj,
            modal: !this.state.modal
        })
    }

    render() {
        const { data } = this.state;
        const data2 = this.state.jsonData.map((prop, key) => {
            return {
                id: key,
                name: prop[0],
                designation: prop[1],
                location: prop[2],
                age: prop[3],
                actions: (
                    // we've added some custom button actions
                    <div className="text-center">
                        {/* use this button to add a edit kind of action */}
                        <Button
                            onClick={() => {
                                let obj = data2.find(o => o.id === key);
                                this.setState({
                                    modal: !this.state.modal,
                                    obj: obj
                                });
                            }}
                            color="inverse"
                            size="sm"
                            round="true"
                            icon="true"
                        >
                            <i className="fa fa-edit" />
                        </Button>
                    </div>
                )
            };
        });
        return <div>
            <Modal isOpen={this.state.modal} toggle={this.toggle}>
                <ModalHeader toggle={this.toggle}>Modal title</ModalHeader>
                <ModalBody>
                    <Form onSubmit={this.handleSubmit}>
                        <Input type="hidden" name="id" id="id" defaultValue={this.state.obj.id} />
                        <FormGroup>
                            <Label for="name">Name</Label>
                            <Input type="text" name="name" id="name" defaultValue={this.state.obj.name} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="designation">Designation</Label>
                            <Input type="text" name="designation" id="designation" defaultValue={this.state.obj.designation} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="location">Location</Label>
                            <Input type="text" name="location" id="location" defaultValue={this.state.obj.location} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="age">Age</Label>
                            <Input type="text" name="age" id="age" defaultValue={this.state.obj.age} />
                        </FormGroup>
                        <FormGroup>
                            <Button color="primary" type="submit">Save</Button>
                            <Button color="secondary" className="ml-1" onClick={this.toggle}>Cancel</Button>
                        </FormGroup>
                    </Form>
                </ModalBody>
            </Modal>


            <Card>
                <CardTitle className="mb-0 p-3 border-bottom bg-light">
                    <i className="mdi mdi-database"></i>นำเข้าข้อมูล</CardTitle>
                <CardBody>
                    <Row>
                        <Col md="10"></Col>
                        <Col md="2">
                            <a href="/customers" className="btn btn-primary"> <i className="fas fa-plus-circle"></i> เพิ่มใหม่</a>
                        </Col>
                    </Row>
                    <br />
                    <ReactTable
                        columns={[
                            {
                                Header: "FirstName",
                                accessor: "name"
                            },
                            {
                                Header: "Designation",
                                accessor: "designation"
                            },
                            {
                                Header: "Location",
                                accessor: "location"
                            },
                            {
                                Header: "Age",
                                accessor: "age"
                            },
                            {
                                Header: "Actions",
                                accessor: "actions",
                                sortable: false,
                                filterable: false
                            }
                        ]}
                        defaultPageSize={10}
                        showPaginationBottom={true}
                        className="-striped -highlight"
                        data={data2}
                        filterable
                    />
                </CardBody>
            </Card>
        </div>
    }
}

export default ShowImportData;
