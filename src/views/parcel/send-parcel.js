import React from 'react';
import {
    Card,
    CardImg,
    CardImgOverlay,
    CardText,
    CardBody,
    CardTitle,
    CardSubtitle,
    CardColumns,
    CardGroup,
    CardDeck,
    CardLink,
    CardHeader,
    CardFooter,
    Row,
    Col,
    Form,
    FormGroup,
    Label,
    Input,
    Button,
    CustomInput,
    InputGroup,
    InputGroupAddon,
} from 'reactstrap';

class SendParcel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
        }
        //this.submitTxt = this.submitTxt.bind(this);
    }

    submitTxt = (event) => {
        event.preventDefault();
        let txt_search = event.target.txt.value;
        console.log(txt_search);
        this.fetchData(txt_search);
    }

    fetchData(txt) {
        let url = "https://porlor.co.th/hc_porlor/ApiMember/getAddressThai?txt_search=";
        console.log(url + txt);
        fetch(url + txt)
            .then(res => res.json())
            .then(result => {
                this.setState({
                    data: result,
                });
                console.log(this.state.data)
            });
    }

    render() {
        return (

            <Row>
                <Col sm="12">
                    <Card className="send-parcel-card">
                        <CardTitle className="p-3 border-bottom mb-0">
                            <i className="mdi mdi-truck-delivery" />
                            <strong> ส่งพัสดุ</strong>
                        </CardTitle>
                        <Form>
                            <CardBody >
                                <Row>
                                    <Col sm="6">
                                        <Card>
                                            <CardHeader tag="h5">
                                                <Row>
                                                    <Col sm="3">
                                                        <Label>ข้อมูลผู้ส่ง</Label>
                                                    </Col>
                                                    <Col sm="9">
                                                        <InputGroup>
                                                            <Input type="text" placeholder="กรอกเลขบัตรประชาชน" />
                                                            <InputGroupAddon addonType="append">
                                                                <Button color="primary">ค้นหา</Button>
                                                            </InputGroupAddon>
                                                        </InputGroup>
                                                    </Col>
                                                </Row>
                                            </CardHeader>
                                            <CardBody>
                                                <Row>
                                                    <Col md="8">
                                                        <FormGroup>
                                                            <Label>ชื่อ-นามสกุล</Label>
                                                            <Input type="text" placeholder="" />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col md="4">
                                                        <FormGroup>
                                                            <Label>เบอร์โทรศัพท์</Label>
                                                            <Input type="text" placeholder="" />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col md="12">
                                                        <FormGroup>
                                                            <Label>ที่อยู่ (เลขที่)</Label>
                                                            <Input type="text" placeholder="" />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col md="12">
                                                        <FormGroup>
                                                            <Form onSubmit={this.submitTxt}>
                                                                <Label>ตำบล/แขวง, อำเภอ/เขต, จังหวัด, รหัสไปรณีย์</Label>
                                                                <InputGroup>
                                                                    <Input type="text" id="txt" name="txt" placeholder="กรอกข้อมูล เช่น อำเภอหรือเขต" />
                                                                    <InputGroupAddon addonType="append">
                                                                        <Button color="primary">ค้นหา</Button>
                                                                    </InputGroupAddon>
                                                                </InputGroup>
                                                            </Form>

                                                        </FormGroup>
                                                    </Col>
                                                </Row>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                    <Col sm="6">
                                        <Card >
                                            <CardHeader tag="h5">
                                                <Row>
                                                    <Col md="3">
                                                        ข้อมูลผู้รับ
                                                    </Col>
                                                    <Col md="3">

                                                    </Col>
                                                    <Col md="6">
                                                        <CustomInput inline type="radio" id="rdoReceiveHouse" name="rdoReceivePlace" label="ส่งที่บ้าน" checked />
                                                        <CustomInput inline type="radio" id="rdoReceiveMySelf" name="rdoReceivePlace" label="ส่งตรงตัว" />
                                                    </Col>
                                                </Row>
                                            </CardHeader>
                                            <CardBody>
                                                <Row>
                                                    <Col md="8">
                                                        <FormGroup>
                                                            <Label>ชื่อ-นามสกุล</Label>
                                                            <Input type="text" placeholder="" />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col md="4">
                                                        <FormGroup>
                                                            <Label>เบอร์โทรศัพท์</Label>
                                                            <Input type="text" placeholder="" />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col md="12">
                                                        <FormGroup>
                                                            <Label>ที่อยู่ (เลขที่)</Label>
                                                            <Input type="text" placeholder="" />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col md="12">
                                                        <FormGroup>
                                                            <Label>ตำบล/แขวง, อำเภอ/เขต, จังหวัด, รหัสไปรณีย์</Label>
                                                            <InputGroup>
                                                                <Input type="text" placeholder="กรอกข้อมูล เช่น อำเภอหรือเขต" />
                                                                <InputGroupAddon addonType="append">
                                                                    <Button color="primary">ค้นหา</Button>
                                                                </InputGroupAddon>
                                                            </InputGroup>
                                                        </FormGroup>
                                                    </Col>
                                                </Row>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm="6">
                                        <Card>
                                            <CardHeader tag="h5">
                                                <Row>
                                                    <Col md="4">
                                                        พัสดุที่จัดส่ง
                                                    </Col>
                                                    <Col md="8">
                                                        <CustomInput inline type="radio" id="rdoCustBusiness" name="rdoCustType" label="ราคาปกติ" checked />
                                                        <CustomInput inline type="radio" id="rdoCustCredit" name="rdoCustType" label="ราคาพิเศษ" />
                                                    </Col>
                                                </Row>
                                            </CardHeader>
                                            <CardBody>
                                                <Row>
                                                    <Col md="4">
                                                        <Row>
                                                            <Col md="12">
                                                                <Label>ประเภทพัสดุ</Label>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col md="12">
                                                                <CustomInput inline type="radio" id="rdoParcelType1" name="rdoParcelType" label="กล่อง" />
                                                                <CustomInput inline type="radio" id="rdoParcelType2" name="rdoParcelType" label="ซองเอกสาร" />
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                    <Col md="3">
                                                        <FormGroup>
                                                            <Label>น้ำหนัก (กก.)</Label>
                                                            <Input type="text" placeholder="" />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col md="5">
                                                        <FormGroup>
                                                            <Label>กว้าง + ยาว + สูง (ซม.)</Label>
                                                            <Input type="text" placeholder="" />
                                                        </FormGroup>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col md="6">
                                                        <Row>
                                                            <Col>
                                                                <div className="form-check form-check-inline">
                                                                    <CustomInput type="checkbox" id="exampleCustomCheckbox1" label="COD (เก็บเงินปลายทาง)" />
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                        <Row>

                                                        </Row>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col md="6">
                                                        <Input type="select" name="ddlInitial">
                                                            <option>--ชื่อธนาคาร--</option>
                                                            <option>กรุงไทย</option>
                                                            <option>กรุงเทพ</option>
                                                            <option>นางสาว</option>
                                                        </Input>
                                                    </Col>
                                                    <Col md="6">
                                                        <Input type="text" placeholder="ระบุเลขที่บัญชี" />
                                                    </Col>
                                                </Row>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                    <Col sm="6">
                                        <Card >
                                            <CardHeader tag="h5">
                                                <Row>
                                                    <Col md="4">
                                                        สรุปค่าขนส่งสุทธิ</Col>
                                                    <Col md="4">
                                                    </Col>
                                                    <Col md="4" className="pull-right">
                                                        <Button type="submit" className="btn btn-success"> <i className="fas fa-print"></i> พิมพ์ลาเบล</Button>
                                                    </Col>
                                                </Row>
                                            </CardHeader>
                                            <CardBody>
                                                <Row>
                                                    <Col md="12">
                                                        <FormGroup>
                                                            <Label>จำนวนเงิน (บาท) </Label>
                                                            <Input type="textarea" rows="3" readOnly />
                                                        </FormGroup>
                                                        {
                                                            /*
                                                             <Button type="submit" className="btn btn-success"> <i className="fas fa-save"></i> บันทึก</Button>
                                                                                                                    <Button type="button" className="btn btn-dark ml-2"><i className="fas fa-redo"></i> ล้างข้อมูล</Button>
                                                            */
                                                        }

                                                    </Col>
                                                </Row>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Form>
                    </Card>
                </Col>
            </Row >
        );
    }
}

export default SendParcel;
